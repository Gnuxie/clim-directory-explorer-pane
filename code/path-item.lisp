#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:nice-directory-explorer)

(defclass path-item ()
  ((mime-type :initarg :mime-type
              :reader mime-type
              :type string)
   
   (absoloute-path :initarg :absolute-path
                   :reader absoloute-path
                   :type pathname)))

(defclass subdirectory-item (path-item) ())

(defun pathname-printing-name (pathname &optional relative-to)
  (if relative-to
      (native-enough-namestring pathname relative-to)
      (native-namestring pathname)))

(defun pretty-pretty-pathname (pathname stream &key icon (relative-to nil))
  (with-output-as-presentation (stream pathname 'clim:pathname :single-box t)
    (when icon (draw-icon stream icon :extra-spacing 3))
    (princ (pathname-printing-name pathname relative-to) stream))
  (terpri stream))

(defun draw-icon (stream pattern &key (extra-spacing 0) )
  (let ((stream (if (eq stream t) *standard-output* stream)))
    (multiple-value-bind (x y) (stream-cursor-position stream)
      (draw-pattern* stream pattern x y)
      (stream-increment-cursor-position stream (+ (pattern-width pattern) extra-spacing) 0))))




