;;; (C) Copyright 2020 by Gnuxie <Gnuxie@protonmail.com>
;;; (C) Copyright 2003 by Andy Hefner (hefner1@umbc.edu)

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the 
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
;;; Boston, MA  02111-1307  USA.

(in-package #:nice-directory-explorer)

;;; mimecase
(defun icon-tree<-mime-assoc (mimes)
  (let ((mime-type-subtype-assoc '()))
    (loop :for (mime icon) :in (remove t mimes :key #'car)
       :do (destructuring-bind (main-type &optional sub-type)
               (split-sequence:split-sequence "/" mime :test #'string=)
             (if sub-type
                 (let ((entry (assoc main-type mime-type-subtype-assoc :test #'string=)))
                   (if entry
                       (push `(,sub-type ,icon) (cdr entry))
                       (push (cons main-type `((,sub-type ,icon)))
                             mime-type-subtype-assoc)))
                 (let ((entry (assoc main-type mime-type-subtype-assoc :test #'string=)))
                   (if entry
                       (push `((t ,icon)) (cdr entry))
                       (push (cons main-type `((t ,icon))) mime-type-subtype-assoc))))))
    
    (push (find t mimes :key #'car) mime-type-subtype-assoc)
    (nreverse mime-type-subtype-assoc)))

(defun alexandria-switch<-subtype-tree (subtype-key subtype-tree)
  `(alexandria:switch (,subtype-key :test #'string=)
     ,@(let* ((otherwise-clause (assoc t subtype-tree))
              (main-cases (remove t subtype-tree :key #'car)))
         (append
          (if (and (= 1 (length main-cases))
                   (not otherwise-clause))
              `((t ,(cadr (first main-cases))))
              main-cases)
          (when otherwise-clause (list otherwise-clause))))))

(defun alexandria-switch<-icon-tree (mtype-key subtype-key icon-tree)
  (flet ((mtype (entry)
           (car entry))

         (first-icon (entry)
           (cadadr entry)))
    `(alexandria:switch (,mtype-key :test #'string=)
       ,@ (let ((otherwise-form (find t icon-tree :key #'car)))
            (append
             (loop :for entry :in (remove t icon-tree :key #'car)
                :collect (if (= 1 (length entry))
                             (list (mtype entry) (first-icon entry))
                             `(,(mtype entry)
                                ,(alexandria-switch<-subtype-tree
                                  subtype-key (cdr entry)))))
             (when otherwise-form (list  otherwise-form)))))))

(defmacro mimecase (keyform &body cases)
  `(destructuring-bind (mtype &optional msubtype)
       (split-sequence:split-sequence "/" ,keyform :test #'string=)
     ,(alexandria-switch<-icon-tree
       'mtype 'msubtype (icon-tree<-mime-assoc cases))))

;;; protocol

(defgeneric icon-of (object icon-set &key)
  (:documentation "Return an icon for this object"))

(defvar *icon-sets* (make-hash-table))
(defvar *icon-cache* (make-hash-table :test 'equal))


(defclass icon-set ()
  ((dispatch-function :initarg :dispatch-function
                      :reader dispatch-function
                      :type function)

   (icon-path :initarg :icon-path
              :reader icon-path)))

(defun %intern-icon-set (name &rest options)
  (setf (gethash name *icon-sets*)
        (apply #'make-instance 'icon-set options)))

(defun find-icon-set (name)
  (gethash name *icon-sets*))

(defmacro define-icon-set (name icon-folder-pathname
                           &body mimecases)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (%intern-icon-set ',name
                       :dispatch-function
                       (lambda (mime-type)
                         (mimecase mime-type
                           ,@mimecases))
                       :icon-path ,icon-folder-pathname)))

(defgeneric icon-pattern (icon-id icon-set)
  (:method ((icon-id pathname) (icon-set icon-set))
    (or (gethash icon-id *icon-sets*)
        (setf (gethash icon-id *icon-sets*)
              (make-pattern-from-bitmap-file
               icon-id
               :format :xpm)))))

(defmethod icon-of ((object string) (icon-set icon-set) &key)
  (let ((matching-icon-name (funcall (dispatch-function icon-set) object)))
    (when matching-icon-name
      (uiop:subpathname (icon-path icon-set)
                        matching-icon-name))))

;;; TODO honestly need some kind of other protocol here but whatever.
(defmethod icon-of ((object (eql :error)) (icon-set icon-set) &key)
  (uiop:subpathname (icon-path (find-icon-set 'standard-icons))
                    "invalid.xpm"))

(defmethod icon-of :around ((mime string) (icon-set icon-set) &key)
  (let ((icon-path (call-next-method)))
    (if icon-path
        (icon-pattern icon-path icon-set)
        (icon-of :error icon-set))))


