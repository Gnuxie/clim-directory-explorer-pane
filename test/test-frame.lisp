#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:nice-directory-explorer.test
  (:use #:clim #:clim-lisp)
  (:local-nicknames (#:nde #:nice-directory-explorer)))

(in-package #:nice-directory-explorer.test)

(define-application-frame test-frame ()
  ()
  (:panes (interactor :interactor)
          (directory-list
           (make-pane 'nde:directory-list
                      :display-function 'nde:display-directory
                      :current-directory (asdf:system-relative-pathname
                                          '#:nice-directory-explorer ""))))
  (:layouts
   (default
       (scrolling (:scroll-bars :vertical)
         directory-list)
       interactor)))

(defmethod nde:get-directory-list ((frame test-frame))
  (find-pane-named frame 'directory-list))

(nde:intern-directory-command test-frame)

(defun launch ()
  (let ((frame  (make-application-frame 'test-frame)))
    (run-frame-top-level frame)))
