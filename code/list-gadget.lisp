#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:nice-directory-explorer)

(defclass directory-list (clim-stream-pane)
  ((current-directory :initform (user-homedir-pathname)
                      :type pathname
                      :initarg :current-directory
                      :accessor current-directory)
   (icon-set :initarg :icon-set
             :accessor icon-set
             :initform (find-icon-set 'standard-icons))))

(defun display-directory (frame directory-list)
  (declare (ignore frame))
  (let ((parent
         (make-instance 'subdirectory-item
                        :mime-type "inode/directory"
                        :absolute-path
                        (uiop:pathname-parent-directory-pathname
                         (current-directory directory-list))))
        (sub-directories
         (mapcar (lambda (d)
                   (make-instance 'subdirectory-item
                                  :mime-type "inode/directory"
                                  :absolute-path d))
                 (uiop:subdirectories (current-directory directory-list))))

        (directory-files
         (mapcar (lambda (f)
                   (make-instance 'path-item
                                  :mime-type (trivial-mimes:mime f)
                                  :absolute-path f))
                 (uiop:directory-files (current-directory directory-list)))))
    (dolist (item (append (list parent)
                          sub-directories
                          directory-files))
      (let ((*standard-output* directory-list))
        (present item)))))


;;; presentation
(define-presentation-method present (path-item (type path-item)
                                               (directory-list directory-list)
                                               view &key)
  (declare (ignore view))
  (pretty-pretty-pathname (absoloute-path path-item)
                          directory-list
                          :icon (icon-of (mime-type path-item)
                                         (icon-set directory-list))
                          :relative-to (current-directory directory-list)))


(defgeneric get-directory-list (frame)
  (:documentation "Application-frames using this pane must implement this method
they can use get-frame-pane."))

(define-command com-change-directory
    ((subdirectory-item subdirectory-item :prompt "Which Directory? "
                :gesture :select))
  (let ((directory-list (get-directory-list *application-frame*)))
    (setf (current-directory directory-list)
          (absoloute-path subdirectory-item))
    (redisplay-frame-pane *application-frame* directory-list :force-p t)))


;;; TODO once https://github.com/McCLIM/McCLIM/pull/1005 has been merged
;;; then we can stop this bs.
(defmacro intern-directory-command (command-table-name)
  (let ((presentation-translator-var 'path-name<-path-item)
        (command-translator-var 'change-directory-command-translator))
    `(progn
       (handler-case
           (add-command-to-command-table 'com-change-directory ',command-table-name
                                         :name t)
         (command-already-present (c) (format t "WARNING: ~a" c)))
       (define-presentation-translator
           ,presentation-translator-var
           (path-item pathname ,command-table-name :gesture t)
           (object)
         (absoloute-path object))
       (define-presentation-to-command-translator ,command-translator-var
           (subdirectory-item com-change-directory ,command-table-name
                      :gesture :select)
           (object)
         (list object)))))
