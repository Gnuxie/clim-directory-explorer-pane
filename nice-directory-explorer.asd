(asdf:defsystem #:nice-directory-explorer
  :license "LGPL-2.1+"
  :description "A CLIM directory explorer incorperating work from the MCCLIM listener.
This is intended to be a gadget that can be used to select a pathname for a command."
  :depends-on ("clim" "trivial-mimes" "split-sequence")
  :serial t
  :components ((:module "code" :components
                        ((:file "package")
                         (:file "util")
                         (:file "icons")
                         (:file "standard-icon-set")
                         (:file "path-item")
                         (:file "list-gadget")))))

