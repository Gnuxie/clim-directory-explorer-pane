#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:nice-directory-explorer)

(define-icon-set standard-icons (asdf:system-relative-pathname
                                 :nice-directory-explorer
                                 "icons/")
  ("application" "object.xpm")
  ("image" "image.xpm")
  ("inode/directory" "folder.xpm")
  ("text" "text.xpm")
  ("text/x-lisp" "lambda.xpm")
  ("text/x-c" "c.xpm")
  ("video" "video.xpm")
  (t "document.xpm"))
