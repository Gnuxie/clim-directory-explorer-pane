#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>

;;; Miscellaneous utilities, UI tools, gross hacks, and non-portable bits.
;;; (C) Copyright 2003 by Andy Hefner (hefner1@umbc.edu)
;;; See toplevel file 'Copyright' for the copyright details.
;;;
|#

(in-package #:nice-directory-explorer)

(defun native-namestring (pathname-designator)
  #+sbcl (sb-ext:native-namestring pathname-designator)
  #+openmcl  (ccl::native-untranslated-namestring pathname-designator)
  #-(or sbcl openmcl) (namestring pathname-designator))

(defun native-enough-namestring (pathname &optional
                                 (defaults *default-pathname-defaults*))
  (native-namestring (enough-namestring pathname defaults)))
